package com.example.ishtefan.permissionshw;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.provider.Settings;


import static android.content.Context.NOTIFICATION_SERVICE;


/**
 * Created by ishtefan on 16.10.2016.
 */

public class MyAirplanReciever extends BroadcastReceiver {

    public static final int ID_AIRPLAN = 123456;
    public static final int NOTIFY_ID_AIRPLAIN = 10;

    String notifyAirplainTitle = "Airplain Mode";
    String notifyAirplainBody;
    String iconAirplain ="R.drawable.ic_flight_black_24dp" ;

    @Override
    public void onReceive(Context context, Intent intent) {

        //        get Airplain Mode status

        if (isAirplaneModeOn(context)) {

            notifyAirplainBody = "Airplain Mode On";

        } else {

            notifyAirplainBody = "Airplain Mode Off";

        }

        Intent notificationIntentAirplane = new Intent(context,MainActivity.class);
        PendingIntent contentIntentAirplain = PendingIntent.getActivity(context, ID_AIRPLAN, notificationIntentAirplane,
                PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationManager nmAirplain = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        Notification.Builder builderAirplain = new Notification.Builder(context);
        builderAirplain.setContentIntent(contentIntentAirplain)
                .setSmallIcon(R.drawable.ic_flight_black_24dp)
                .setAutoCancel(true)
                .setContentTitle(notifyAirplainTitle)
                .setContentText(notifyAirplainBody);
        Notification nAirplain = builderAirplain.build();
        nmAirplain.notify(NOTIFY_ID_AIRPLAIN, nAirplain);

    }


    /**
     * Gets the state of Airplane Mode.
     *
     * @param context
     * @return true if enabled.
     */

    private static boolean isAirplaneModeOn(Context context) {

        return Settings.System.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;

    }

/*    public class NotificationDelete {

        NotificationDelete notificationDelete = new NotificationDelete();
        public void deleteNotificatoin(String you_notify_id) {

            notificationManager.cancel(you_notify_id)
        }
    }*/
}



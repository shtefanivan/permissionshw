package com.example.ishtefan.permissionshw;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.widget.Toast;

import static android.content.Context.NOTIFICATION_SERVICE;
import static android.content.Context.WIFI_SERVICE;

/**
 * Created by ishtefan on 17.10.2016.
 */
public class MyWiFiReciever extends BroadcastReceiver {
    public static final int ID_WIFI = 12345;
    public static final int NOTIFY_ID_WIFI = 12;
    String notifyTitle = "WiFi";
    String notifyBody;
    String iconWiFi;

    @Override
    public void onReceive(Context context, Intent intent) {

//        get WiFi status
        context.getSystemService(WIFI_SERVICE);
        WifiManager wifiManager = (WifiManager) context.getSystemService(WIFI_SERVICE);
        int wifi = wifiManager.getWifiState();

        switch (wifi) {
            case 1:
                notifyBody = "WiFi Off";
                iconWiFi = "";
                break;

            case 2:
                notifyBody = "WiFi Connecting...";
                break;

            case 3:
                notifyBody = "WiFi On";
                break;
            default:
                notifyBody = "ХЗ";
        }

/*
        context.getSystemService(Context.NETWORK_STATS_SERVICE);
        AccessibilityManager accessibilityManager =
                (AccessibilityManager) context.getSystemService(Context.ACCESSIBILITY_SERVICE);
        accessibilityManager.getEnabledAccessibilityServiceList(ID_WIFI);
*/

        Intent notificationIntent = new Intent();
        PendingIntent contentIntent = PendingIntent.getActivity(context, ID_WIFI, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationManager nm = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.ic_wifi_black_24dp)
                .setAutoCancel(true)
                .setContentTitle(notifyTitle)
                .setContentText(notifyBody);
        Notification n = builder.build();
        nm.notify(NOTIFY_ID_WIFI, n);

        Toast.makeText(context, "Большой брат следит за тобой", Toast.LENGTH_SHORT).show();


    }
}
